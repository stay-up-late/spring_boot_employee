package com.ning.spring_boot_employee.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.servlet.Filter;
import javax.sql.DataSource;
import java.util.HashMap;
//druid监控，还有广告就离谱。
@Configuration
public class DruidConfig {
    @ConfigurationProperties(prefix ="spring.datasource")
    @Bean
    public DataSource druidDataSource(){
        return new DruidDataSource();
    }
    //后台监控：相当于web.xml  因为SpringBoot内置了servle容器，所以没有web.xml，替代方法：ServletRegistrationBean
    @Bean
    public ServletRegistrationBean statViewServlet(){
        ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
        //后台要有人登录
        HashMap<String, String> initParameters = new HashMap<>();
        //允许谁可以访问
        initParameters.put("allow",""); //第二个为空则所有人都可以访问
        //禁止谁访问
//        initParameters.put("wcn","192.xxx.xxx.xxx");
        //增加配置
        initParameters.put("loginUsername","admin");//登录key是固定的 loginUsername  loginUsername
        initParameters.put("loginPassword","123456");
        bean.setInitParameters(initParameters);//设置初始化参数
        return bean;
    }
    HashMap<String, String> initParameters = new HashMap<>();
//filter 过滤
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean<Filter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new WebStatFilter());   //WebStatFilter()这是阿里巴巴的
        //可以过滤那些请求呢？？
        HashMap<String, String> initParameters = new HashMap<>();
        //这些东西不进行统计
        initParameters.put("exclusions","*.js,*,.css,/druid/*");
        bean.setInitParameters(initParameters);
        return bean;
    }
}


