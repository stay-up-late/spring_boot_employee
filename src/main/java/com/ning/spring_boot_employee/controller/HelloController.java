package com.ning.spring_boot_employee.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Controller
@RestController //返回字符串
public class HelloController {
    @RequestMapping("/hello")
    public String hello() {
        //调用接口，接受前端参数
        String aaa = new String("aaa");
        return aaa;
    }
}
