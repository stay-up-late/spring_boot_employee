package com.ning.spring_boot_employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Map;

@RestController
public class JDBCController {
    @Autowired  //前面忘了加，结果一直报错NULL 一定要依赖注入！！
    JdbcTemplate jdbcTemplate;

    //查询数据库所有信息
    @GetMapping("/userList")
    public List<Map<String, Object>> userList(){
        String sql = "select * from spring_boot_employee.emp";
        List<Map<String, Object>> List_maps = jdbcTemplate.queryForList(sql);
        return List_maps;
    }
    @GetMapping("/addUser")//添加数据
    public String adduser(){
        String sql = "insert into spring_boot_employee.emp(id,name,pwd) values(5,'撒酒疯',666666)";
        jdbcTemplate.update(sql);
        return "update ok!";
    }
    @GetMapping("/updateUser/{id}")//通过id修改数据库
    public String updateUser(@PathVariable("id") int id){
        String sql = "update spring_boot_employee.emp set name=?,pwd=? where id="+id;
        //封装
        Object[] objects = new Object[2];
        objects[0] = "小明啊";
        objects[1] = "123123123";
        jdbcTemplate.update(sql,objects);
        return "updateUser ok!";
    }
    @GetMapping("/deleteUser/{id}")//通过id删除数据库
    public String deleteUser(@PathVariable("id") int id){
        String sql = "delete from spring_boot_employee.emp where id=?";
        jdbcTemplate.update(sql,id);
        return "deleteUser ok!";
    }


}
